import React, { Component } from 'react'
import {Card} from 'react-bootstrap'

export default class View extends Component {
    render() {
        let id = this.props.match.params.id;
        return (
            <div className="container"><br/>
                <h3>Article</h3>
            <Card>
                <div className="row no-gutters">
                <div className="col-4">  
                <Card.Img variant="top" src="https://d.newsweek.com/en/full/1176971/obesity-meme.png?w=1600&h=900&q=88&f=0c8293185b292dc0d5eb47eace629bd1" />
                </div>
                    <div className="col-8">
                    <Card.Body>
                    <Card.Title>Card title</Card.Title>
                    <Card.Text>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</Card.Text>
                    </Card.Body>
                    </div>
                </div>
            </Card>
            </div>
        )
    }
}

