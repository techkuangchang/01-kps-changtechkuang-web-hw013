import React, { useState } from 'react'
import axios from 'axios'

function UploadPic() {
    const [image , setImage] = useState('')
    const [loading, setLoading] = useState(false)

    const uploadImage = async event => {
        const files = event.target.files
        const data = new FormData()
        data.append('file',files[0])
        data.append('upload_preset','')
        setLoading(true)
        
        axios.post(`http://110.74.194.124:15011/v1/api/articles`,data)
        .then(res => {
            console.log(res.data);
        }) 
        const file = await axios
        
        setImage(file.secure_url)
        setLoading(false)
    }

    return (
        <div>
            <h3>Upload Image</h3>
            <input
                type="file"
                name="file"
                placeholder="Upload an image"
                onChange={uploadImage}
            />
            {loading ? (
                <h3>Loading...</h3>
            ) : (
                <img src={image} style={{ width: '120px', height:"120px"}} />
            )}
        </div>
    )
}

export default UploadPic
