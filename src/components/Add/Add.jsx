import React, { Component } from 'react'
import {Form,Button} from 'react-bootstrap';
import axios from 'axios';
import UploadPic from './UploadPic';

const initialState = {
    TITLE: '',
    DESCRIPTION: '',
    TITLEERORR: '',
    DESCRIPTIONERORR: '',
}

export default class Add extends Component {
    
    state = initialState;

    validate = () => {
        let TITLEERORR = "";
        let DESCRIPTIONERORR = "";

        if(!this.state.TITLE){
            TITLEERORR = "* Title can not be blank";
        }

        if(!this.state.DESCRIPTION){
            DESCRIPTIONERORR = "* Description can not be blank";
        }

        if(TITLEERORR || DESCRIPTIONERORR){
            this.setState({TITLEERORR,DESCRIPTIONERORR});
            return false;
        }
        return true;
    }

    handleChange = event => {
        this.setState({     
            [event.target.name] : event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        const isValid = this.validate();
    
    if(isValid){ 
        const article = {
            TITLE: this.state.TITLE,
            DESCRIPTION: this.state.DESCRIPTION,
        }   
        axios.post(`http://110.74.194.124:15011/v1/api/articles`,article)
        .then(res => {
            console.log(res.data);
        })    
        alert("Article Add Succesfully");
        //clear
        this.setState(initialState);
    }    
    }

    render() {
        return (
            <div className="container"><br/>
            <h3>Add Article</h3>
                <div className="row">
                <div className="col-7">
                <Form onSubmit={this.handleSubmit}>
                <Form.Label>TITLE</Form.Label>
                <Form.Control type="text" value={this.state.TITLE} name="TITLE" placeholder="Enter Title" onChange={this.handleChange}/>
                <Form.Text style={{color:"red"}}>
                    {this.state.TITLEERORR}
                </Form.Text>
                <Form.Label>Description</Form.Label>
                <Form.Control type="text" value={this.state.DESCRIPTION} name="DESCRIPTION" placeholder="Enter Description" onChange={this.handleChange}/>
                <Form.Text style={{color:"red"}}>
                    {this.state.DESCRIPTIONERORR}
                </Form.Text>
                    <br/>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
                </Form>
                </div>

                <div className="5">
                    <UploadPic/>
                </div>

                </div>
            </div>
        )
    }
}
