import React from 'react'
import {Nav,Navbar,FormControl,Form,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function Navs() {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <div className="container">  
                <Navbar.Brand>AMS</Navbar.Brand>  
                <Nav className="mr-auto">
                <Nav.Link as = {Link} to ="/home">Home</Nav.Link>
                </Nav>
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-info">Search</Button>
                </Form>
                </div>
            </Navbar>
        </div>
    )
}

export default Navs
