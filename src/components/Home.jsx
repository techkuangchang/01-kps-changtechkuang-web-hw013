import React, { Component } from 'react'
import {Button,Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import 'moment-timezone';
import Moment from 'react-moment';
import axios from 'axios';

export default class Home extends Component {
    state = {
        info: [],
    };

    componentWillMount(){
        console.log("Will Mount");
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res)=>{
            console.log(res.data.DATA);
            this.setState({
                info: res.data.DATA,
            });
        });
    }

    myEdit= (infos) => {
        
    }

    render() {
        return (
            <div>
                <div className="text-center">
                <h1 className="text-center">Article Management</h1>
                <Button variant="outline-info" >
                    <Link to="/add">Add Article</Link>
                </Button>&nbsp;
                {/* Button in Table */}
                <Button variant="success">
                    <Link to={`/view/1`}>View</Link>
                </Button>&nbsp;
                <Button variant="warning" onClick={()=> this.Edit}>Edit</Button>&nbsp;
                <Button variant="danger" onClick={()=> this.Delete}>Delete</Button>
                {/*  */}
                </div>

                <br/>

                <div className="container-fliud">
                <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>TITLE</th>
                    <th>DESCRIPTION</th>
                    <th>CREATE DATE</th>
                    <th>IMAGE</th>
                    <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.info.map((infos) => (
                        <tr key={infos.ID}>
                            <td>{infos.ID}</td>
                            <td>{infos.TITLE}</td>
                            <td className="w-50 p-3">{infos.DESCRIPTION}</td>
                            <td>
                                <Moment format="YYYY-MM-DD">{infos.CREATED_DATE.localtime}</Moment>
                            </td>
                            <td><img src={infos.IMAGE} width="120" height="120" alt=""/></td>
                            <td>
                                
                                
                            </td>
                        </tr>
                    ))}
                </tbody>
                </Table>
                </div>

            </div>
        );
    }
}
